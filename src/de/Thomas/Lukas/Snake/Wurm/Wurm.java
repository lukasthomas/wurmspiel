package de.Thomas.Lukas.Snake.Wurm;

import de.Thomas.Lukas.Snake.Control.Steuerung;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Wurm {

    private Zelle[] wurmZelle;
    public static BufferedImage wurmImg;
    public static BufferedImage wurmHeadImg;
    public static BufferedImage fressenImg;
    private static int aLaenge;
    private int aRichtung;


    public Wurm() {
        aLaenge = 4;
        readImmages();
        erzeugeWurm(7, 7);
    }


    private void readImmages() {
        File WurmBild = new File("bilder/wurm.png");
        File HeadBild = new File("bilder/head.png");
        File FressenBild = new File("bilder/futter.png");

        try {
            wurmImg = ImageIO.read(WurmBild);
            wurmHeadImg = ImageIO.read(HeadBild);
            fressenImg = ImageIO.read(FressenBild);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public int gibKopfPosX() {
        return wurmZelle[0].x;
    }

    public int gibKopfPosY() {
        return wurmZelle[0].y;
    }

    public boolean krieche() {

        if (collision(aRichtung, wurmZelle[0].x, wurmZelle[0].y)) {
            Zelle.playSound("sound/fail.au");
            return false;
        }

        for (int j = aLaenge - 1; j > 0; j--) {
            wurmZelle[j].x = wurmZelle[j - 1].x;
            wurmZelle[j].y = wurmZelle[j - 1].y;
        }

        switch (aRichtung) {
            case Steuerung.LEFT:
                wurmZelle[0].x--;
                break;
            case Steuerung.RIGHT:
                wurmZelle[0].x++;
                break;
            case Steuerung.UP:
                wurmZelle[0].y--;
                break;
            case Steuerung.DOWN:
                wurmZelle[0].y++;
                break;
        }
        if (aLaenge >= 10) {
            return false;
        }

        return true;
    }

    private boolean collision(int richtung, int x, int y) {
        switch (richtung) {
            case Steuerung.LEFT:
                if (onWurm(x - 1, y) || x == 1) {
                    return true;
                }
                break;
            case Steuerung.RIGHT:
                if (onWurm(x + 1, y) || x == 10) {
                    return true;
                }
                break;
            case Steuerung.UP:
                if (onWurm(x, y - 1) || y == 1) {
                    return true;
                }
                break;
            case Steuerung.DOWN:
                if (onWurm(x, y + 1) || y == 10) {
                    return true;
                }
                break;
        }
        return false;
    }

    public void wachse() {

        Zelle[] newZelle = new Zelle[aLaenge + 1];

        for (int j = 0; j < aLaenge; j++) {
            newZelle[j] = wurmZelle[j];
        }

        newZelle[wurmZelle.length] = new WurmZelle(wurmZelle[aLaenge - 1].x, wurmZelle[aLaenge - 1].y);
        wurmZelle = newZelle;
        Zelle.playSound("sound/eat.au");
        aLaenge++;

        if (aLaenge >= 10) {
            System.out.println("gewonnen");
            Zelle.playSound("sound/win.au");
        }
    }

    public boolean onWurm(int x, int y) {
        for (Zelle zellen : wurmZelle) {
            if (zellen.hatGleicheXY(x, y)) {
                return true;
            }
        }
        return false;
    }

    public void setaRichtung(int pRichtung) {

        switch (aRichtung) {
            case Steuerung.LEFT:
                if (pRichtung == Steuerung.RIGHT) {
                    return;
                }
                break;

            case Steuerung.RIGHT:
                if (pRichtung == Steuerung.LEFT) {
                    return;
                }
                break;

            case Steuerung.UP:
                if (pRichtung == Steuerung.DOWN) {
                    return;
                }
                break;

            case Steuerung.DOWN:
                if (pRichtung == Steuerung.UP) {
                    return;
                }
                break;
        }

        this.aRichtung = pRichtung;
    }

    public void draw(Graphics g, int breite, int hoehe) {
        for (Zelle zellen : wurmZelle) {
            zellen.zeichne(g, breite, hoehe);
        }
    }

    public void erzeugeWurm(int startX, int startY) {

        aRichtung = Steuerung.DEFAULT;

        int pX = startX;
        int pY = startY;

        wurmZelle = new Zelle[aLaenge];

        wurmZelle[0] = new KopfZelle(pX, pY);
        for (int i = 1; i < aLaenge; i++) {
            wurmZelle[i] = new WurmZelle(pX + i, pY);
        }
    }

    public Zelle[] getWurmPos() {
        return wurmZelle;
    }

    public static int getaLaenge(){
        return aLaenge;
    }

}
