package de.Thomas.Lukas.Snake.Wurm;


import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class Zelle {

    protected int x;
    protected int y;

    public Zelle(int pX, int pY) {
        this.x = pX;
        this.y = pY;
    }

    public boolean hatGleicheXY(int pX, int pY) {
        if (pX == x && pY == y) {
            return true;
        }
        return false;
    }

    public abstract void zeichne(Graphics g, int breite, int hoehe);

    protected BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }

    public static void playSound(String pathToSound) {
        try {
            String gongFile = pathToSound;
            InputStream in = null;
            in = new FileInputStream(gongFile);
            AudioStream audioStream = new AudioStream(in);
            AudioPlayer.player.start(audioStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
