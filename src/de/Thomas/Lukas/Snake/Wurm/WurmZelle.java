package de.Thomas.Lukas.Snake.Wurm;

import java.awt.*;

public class WurmZelle extends Zelle{

    public WurmZelle(int pX, int pY) {
        super(pX, pY);
    }

    @Override
    public void zeichne(Graphics g, int breite, int hoehe) {
        g.setColor(Color.BLACK);
        g.drawImage(resize(Wurm.wurmImg, breite/12, hoehe/12), x*(breite/12), y*(hoehe/12), null);
        //g.fillRect(x*(breite/12), y*(hoehe/12), breite/12, hoehe/12);
    }

}
