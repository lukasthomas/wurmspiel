package de.Thomas.Lukas.Snake.Wurm;


import java.awt.*;

public class KopfZelle extends Zelle{


    public KopfZelle(int pX, int pY) {
        super(pX, pY);
    }

    @Override
    public void zeichne(Graphics g, int breite, int hoehe) {
        g.setColor(Color.BLUE);
        g.drawImage(resize(Wurm.wurmHeadImg, breite/12, hoehe/12), x*(breite/12), y*(hoehe/12), null);
        //g.fillRect(x*(breite/12), y*(hoehe/12), breite/12, hoehe/12);
    }
}
