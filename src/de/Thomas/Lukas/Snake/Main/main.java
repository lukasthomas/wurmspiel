package de.Thomas.Lukas.Snake.Main;

import de.Thomas.Lukas.Snake.Graphics.Oberflaeche;

public class main {

    public static final int BREITE = 600;
    public static final int HOEHE = 600;


    public static void main(String[] args) {
        System.out.println("------- SNAKE -------");
        System.out.println("Author: Lukas Thomas");
        System.out.println("Version: 0.0.1");

        new Oberflaeche(BREITE, HOEHE);
    }
}

