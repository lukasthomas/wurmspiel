package de.Thomas.Lukas.Snake.Control;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MeinKeyManager extends KeyAdapter {

    private int currentKey;
    private Steuerung steuerung;

    public MeinKeyManager(Steuerung steuerung) {
        this.steuerung = steuerung;
    }

    public void keyPressed(KeyEvent e){
        currentKey = e.getKeyCode();
        steuerung.verarbeiteTastenDruck();
    }

    public int getMyKey() {
        return currentKey;
    }
}