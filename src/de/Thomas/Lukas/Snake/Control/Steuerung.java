package de.Thomas.Lukas.Snake.Control;

import de.Thomas.Lukas.Snake.Graphics.Oberflaeche;
import de.Thomas.Lukas.Snake.Main.main;
import de.Thomas.Lukas.Snake.Time.Uhr;
import de.Thomas.Lukas.Snake.Wurm.Wurm;
import de.Thomas.Lukas.Snake.Wurm.FutterZelle;
import de.Thomas.Lukas.Snake.Wurm.Zelle;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Random;


public class Steuerung{

    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int UP = 2;
    public static final int DOWN = 3;

    public static final int DEFAULT = LEFT;

    private Wurm wurm;
    private Uhr uhr;
    private Oberflaeche oberflaeche;

    private Zelle [] wurmPos;
    private FutterZelle futterZelle;

    private Random rnd;

    public Steuerung(Oberflaeche oberflaeche) {

        this.oberflaeche = oberflaeche;
        wurm = new Wurm();
        wurmPos=wurm.getWurmPos();
        rnd = new Random();
        erzeugeFutter();
        uhr = new Uhr(this);
    }


    public void verarbeiteTastenDruck(){
        int key = oberflaeche.getMeinKeyManager().getMyKey();
        if(key == KeyEvent.VK_LEFT){
            wurm.setaRichtung(LEFT);
        } else if(key == KeyEvent.VK_RIGHT){
            wurm.setaRichtung(RIGHT);
        } else if(key == KeyEvent.VK_UP){
            wurm.setaRichtung(UP);
        } else if(key == KeyEvent.VK_DOWN){
            wurm.setaRichtung(DOWN);
        }
    }

    public void zeichneFutterundWurm(Graphics g, int breite, int hoehe){
        futterZelle.zeichne(g, breite, hoehe);
        wurm.draw(g, breite, hoehe);
    }

    public void verarbeiteUhrTick(){
        if(!wurm.krieche()){
            uhr.stoppe();
            System.out.println("Spiel vorbei!");
        }

        if(futterZelle.hatGleicheXY(wurm.gibKopfPosX(), wurm.gibKopfPosY())){
            erzeugeFutter();
            wurm.wachse();
        }

        oberflaeche.repaint();

    }

    private void erzeugeFutter(){

        int x;
        int y;

        do {
            x = rnd.nextInt(9)+1;
            y = rnd.nextInt(9)+1;
        } while (wurm.onWurm(x, y));

        futterZelle = new FutterZelle(x, y);
    }
}
