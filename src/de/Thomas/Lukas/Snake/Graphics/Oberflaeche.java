package de.Thomas.Lukas.Snake.Graphics;

import de.Thomas.Lukas.Snake.Control.MeinKeyManager;
import de.Thomas.Lukas.Snake.Control.Steuerung;
import de.Thomas.Lukas.Snake.Main.main;
import de.Thomas.Lukas.Snake.Wurm.Wurm;

import javax.swing.*;
import java.awt.*;

public class Oberflaeche extends JPanel {

    private Graphics g;
    private Steuerung steuerung;
    private MeinKeyManager meinKeyManager;
    private JLabel punktestand;

    public Oberflaeche(int Breite, int Hoehe) {
        steuerung = new Steuerung(this);
        new SpielFenster(Breite, Hoehe, this, meinKeyManager = new MeinKeyManager(steuerung));
    }


    public MeinKeyManager getMeinKeyManager() {
        return meinKeyManager;
    }

    public void paintComponent(Graphics g) {
        g.drawRect(getWidth() / 12, getHeight() / 12, getWidth() - getWidth() / 12 * 2, getHeight() - getHeight() / 12 * 2);
        steuerung.zeichneFutterundWurm(g, getWidth(), getHeight());
        punktestand.setText("Punkte " + Wurm.getaLaenge());
    }

    private class SpielFenster extends JFrame {

        public SpielFenster(int Breite, int Hoehe, Oberflaeche oberflaeche, MeinKeyManager meinKeyManager) {
            setSize(Breite, Hoehe);
            setTitle("Lukas's Wurmspiel");
            setLocationRelativeTo(null);
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            setContentPane(oberflaeche);
            addKeyListener(meinKeyManager);
            add(punktestand = new JLabel("Punkte " + Wurm.getaLaenge()));
            setVisible(true);
            setFocusable(true);
            requestFocus();
        }

    }


}
