package de.Thomas.Lukas.Snake.Time;

import de.Thomas.Lukas.Snake.Control.Steuerung;

import javax.swing.*;


public class Uhr {

    private Timer timer;
    private Steuerung steuerung;

    private double aTakt = 0.2;

    public Uhr(Steuerung steuerung) {
        this.steuerung = steuerung;
        starte();
    }

    public void starte(){

        timer = new Timer((int)(aTakt*1000), e -> steuerung.verarbeiteUhrTick());
        timer.start();
    }

    public void stoppe(){
        timer.stop();
    }

}
